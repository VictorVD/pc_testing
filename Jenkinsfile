pipeline {
  agent any
  options {
    timeout(time: 1, unit: 'HOURS')
    disableConcurrentBuilds()
    timestamps()
  }
  triggers{
    bitbucketpr(projectPath:'https://VictorVD@bitbucket.org/VictorVD/pc_testing',
    cron:'H/15 * * * *',
    credentialsId:'bitbucket_vd_credentials',
    username:'',
    password:'',
    repositoryOwner:'VictorVD',
    repositoryName:'pc_testing',
    branchesFilter:'',
    branchesFilterBySCMIncludes:false,
    ciKey:'PCAServer',
    ciName:'PCAutomation',
    ciSkipPhrases:'',
    checkDestinationCommit:false,
    approveIfSuccess:true,
    cancelOutdatedJobs:true,
    commentTrigger:'This should work, probably ```\\_(o_o)_/```')
  }
  stages {
    stage('Validation') {
      options {
        timeout(time: 20, unit: 'MINUTES')
      }
      steps {
        sh 'mvn -N io.takari:maven:wrapper'
        sh './mvnw validate'
      }
    }
    stage('Build') {
      steps {
        sh './mvnw clean compile -Dmaven.test.skip=true'
      }
    }
    stage('Test') {
      steps {
        sh './mvnw test'
      }
      post {
        always {
          sh 'echo "Gathering test results"'
          junit '**/target/surefire-reports/*.xml'
        }
        aborted {
          echo 'Test stage has been aborted. Perhaps, because time ends up.'
        }
        failure {
          echo 'Something goes wrong.'
        }
      }
    }
    stage('Archiving') {
      steps {
        sh './mvnw package -Dmaven.test.skip=true'
        sh 'mv $(find . -regextype "egrep" -regex ".*petclinic.*\\.jar$") ./target/${GIT_BRANCH#*/}_$(date +"%F_%H-%M").jar'
        script {
          def date = new Date()
          currentBuild.displayName=date.format("yyyy-MM-dd_H")
        }
        s3Upload consoleLogLevel: 'INFO',
        dontWaitForConcurrentBuildCompletion: false,
        entries: [[bucket: 'davydov-petclinic-artifacts/petclinic_artifacts',
        excludedFile: '', flatten: true, gzipFiles: false, keepForever: false,
        managedArtifacts: false, noUploadOnFailure: true,
        selectedRegion: 'us-west-1', showDirectlyInBrowser: true,
        sourceFile: '**/target/*.jar', storageClass: 'STANDARD',
        uploadFromSlave: false, useServerSideEncryption: false]],
        pluginFailureResultConstraint: 'FAILURE',
        profileName: 'uploader', userMetadata: []
      }
    }
    stage('Deploy') {
      when {
        anyOf {
          branch 'release'
          branch 'master'
        }
      }
      steps {
        sh 'echo "Deploying of the application from branch {$GIT_BRANCH#*/} starts here."'
      }
    }
  }
}
