#!/bin/bash

tagname=${1:-"tmp"}
version=${2:-""}
logfile=${3:-"log"}

if [ -n "$version" ]; then
  logfile="$logfile-$tagname-$version.txt"
  tagname="$tagname:$version"
else
  logfile="$logfile-$tagname.txt"
fi

docker build -t $tagname . | tee $logfile;
